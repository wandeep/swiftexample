//
//  TeamListViewController.swift
//  SwiftExample
//
//  Created by Wandeep Basra on 10/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import UIKit

/**
 TeamListViewController - displays a list of teams for the selected league retrieved via the web request in a UITableView
 */

class TeamListViewController: UITableViewController {
    
    private let cellReuseIdentifier: String = "teamCell"
    
    // Team List View Model
    private var teamListViewModel : TeamListViewModel
    
    // Initializer - inject any dependencies
    init(withTeamListViewModel teamListViewModel: TeamListViewModel){
        self.teamListViewModel = teamListViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = self.teamListViewModel.leagueNameTitle()
        
        // Add refresh control to table view
        let refreshControl = UIRefreshControl()
        let refreshControlText = NSLocalizedString("TEAM_LIST_VIEW_REFRESH_CONTROL", comment:"Refreshing")
        refreshControl.attributedTitle = NSAttributedString(string: refreshControlText)
        refreshControl.addTarget(self, action: #selector(TeamListViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        self.refreshControl = refreshControl
        
        // Request the data - use UIRefreshController to indicate asynchronous request in progress
        self.refreshControl?.beginRefreshing()
        self.fetchTeamList()
    }
    
    // MARK: Private
    
    private func fetchTeamList() {
        // Asynchronously fetch the team list, on completion update table view or display error message
        self.teamListViewModel.getTeamList(completion: { (error) in
            // Stop the refresh control (if it's currently refreshing)
            if (self.refreshControl?.isRefreshing)! {
                self.refreshControl?.endRefreshing()
            }
            // Check the result of the team list fetch
            if error != nil {
                // Error occured, so display an error alert
                let alertTitle = NSLocalizedString("ALERT_CONTROLLER_ERROR_TITLE", comment:"Error")
                let alertMessage = error?.localizedDescription
                self.displayAlertWithTitle(title: alertTitle, message: alertMessage!)
                
            } else {
                // No error, so refresh table view
                self.tableView.reloadData()
            }
        })
    }
    
    // Helper method to display an UIAlertController with the passed in title/message
    private func displayAlertWithTitle(title: String, message: String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("ALERT_CONTROLLER_CANCEL_ACTION_TITLE", comment:"OK"),
                                         style: .cancel, handler: nil)
        
        alert.addAction(cancelAction)
        self.present(alert, animated: true)
    }
    
    // MARK: UIRefreshControl handler
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        // Update the team list
        self.fetchTeamList()
        // End the refresh control
        refreshControl.endRefreshing()
    }
    
    // MARK: UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.teamListViewModel.numberOfItemsToDisplay(in: section))
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) ?? UITableViewCell(style:UITableViewCellStyle.subtitle, reuseIdentifier: cellReuseIdentifier)
        // Use the TeamListViewModel to retrieve the formatted strings to be displayed in the cell
        cell.textLabel!.text = self.teamListViewModel.titleToDisplay(for: indexPath)
        cell.detailTextLabel!.text = self.teamListViewModel.subtitleToDisplay(for: indexPath)
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // On cell selection, check for a non-nil website before transitioning to the
        // Teams Detail View Controller for the selected team cell
        if let website = self.teamListViewModel.teamWebsite(for: indexPath) {
            // Get the team name
            let teamName = self.teamListViewModel.teamName(for: indexPath) ?? ""
            // Initialise the TeamDetailViewModel (to be injected into Team Detail View Controller)
            let teamDetailViewModel = TeamDetailViewModel(withName: teamName, webpageURLString: website)
            // Instantiate the Team Detail View Controller and inject team detail view model
            let teamDetailViewController = TeamDetailViewController(withTeamDetailViewModel: teamDetailViewModel)
            // Push onto Navigation Controller stack
            self.navigationController?.pushViewController(teamDetailViewController, animated: true)
            // Clear the UINavigationItem 'back' button text - so you don't see "< leagueName" on the team detail view controller
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            // De-select row from table view
            self.tableView.deselectRow(at: indexPath, animated: false)
        }
    }
    
    deinit {
        print("TeamListViewController deinit")
    }
}
