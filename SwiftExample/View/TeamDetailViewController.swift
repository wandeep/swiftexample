//
//  TeamDetailViewController.swift
//  SwiftExample
//
//  Created by Wandeep Basra on 10/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import UIKit

/**
 TeamDetailViewController - attempts to load and display the website for the slected team in an embedded UIWebView
 */

class TeamDetailViewController: UIViewController, UIWebViewDelegate {
    
    // UI
    var webView: UIWebView?
    var activityIndicatorView : UIActivityIndicatorView?
    
    // Team Detail View Model
    private var teamDetailViewModel : TeamDetailViewModel
    
    // Initializer - inject any dependencies
    init(withTeamDetailViewModel teamDetailViewModel: TeamDetailViewModel){
        self.teamDetailViewModel = teamDetailViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.edgesForExtendedLayout = []
        
        // Set title to the selected team name
        self.title = self.teamDetailViewModel.name

        // Add web view - ensure we set ourself as delegate
        let webView = UIWebView()
        webView.scalesPageToFit = true
        webView.delegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(webView)
        self.webView = webView
        
        // Layout constraints for web view (using Visual Format Language) - layout fullscreen to superview
        let views = ["webView" : webView]
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[webView]|", options: [], metrics: nil, views: views)
        self.view.addConstraints(horizontalConstraints)
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[webView]|", options: [], metrics: nil, views: views)
        self.view.addConstraints(verticalConstraints)
        
        // Add activity indicator view - center in superview BUT ensure it is above the webview
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicatorView.center = view.center
        self.view.insertSubview(activityIndicatorView, aboveSubview: webView)
        self.activityIndicatorView = activityIndicatorView
        
        // Load the team webpage
        self.loadTeamWebpage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("TeamDetailViewController deinit")
        self.webView?.stopLoading()
        self.webView?.delegate = nil
    }
    
    // MARK: Private
    
    // Attempt to load the webpage for the selected team
    private func loadTeamWebpage() {
        print("load team webpage at \(teamDetailViewModel.webpageURLString)")
        if let url = URL(string: teamDetailViewModel.webpageURLString) {
            self.webView?.loadRequest( URLRequest(url: url) )
        } else {
            // Webpage URL is invalid
            self.displayMessage(message: NSLocalizedString("TEAM_WEB_VIEW_ERROR", comment:"Error"))
        }
    }
    
    // Helper method to display message in embedded UIWebView
    private func displayMessage(message: String) {
        self.webView?.loadHTMLString("<h1>\(message)</h1>", baseURL: nil)
    }
    
    // MARK: UIWebViewDelegate methods
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        print("webViewDidStartLoad()")
        // Start the activity indicator view
        self.activityIndicatorView?.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("webViewDidFinishLoad()")
        // Stop the activity indicator view
        self.activityIndicatorView?.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("webView: didFailLoadWithError: error - \(error.localizedDescription)")
        // Stop the activity indicator view
        self.activityIndicatorView?.stopAnimating()
        // Display error message
        let errorMessage = "ERROR!!! URL: \(teamDetailViewModel.webpageURLString) failed with error - \(error.localizedDescription)"
        self.displayMessage(message: errorMessage)
    }
}
