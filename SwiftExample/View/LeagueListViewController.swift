//
//  LeagueListViewController.swift
//  SwiftExample
//
//  Created by Wandeep Basra on 07/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import UIKit

/**
 LeagueListViewController - displays a list of leagues retrieved via the web request in a UITableView
 */

class LeagueListViewController: UITableViewController {

    private let cellReuseIdentifier: String = "leagueCell"
    
    // League List View Model
    private var leagueListViewModel : LeagueListViewModel
    
    // Initializer - inject any dependencies
    init(withLeagueListViewModel leagueListViewModel: LeagueListViewModel){
        self.leagueListViewModel = leagueListViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = NSLocalizedString("LEAGUE_LIST_VC_TITLE", comment:"League List VC Title")
        
        // Add refresh control to table view
        let refreshControl = UIRefreshControl()
        let refreshControlText = NSLocalizedString("LEAGUE_LIST_VIEW_REFRESH_CONTROL", comment:"Refreshing")
        refreshControl.attributedTitle = NSAttributedString(string: refreshControlText)
        refreshControl.addTarget(self, action: #selector(LeagueListViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        self.refreshControl = refreshControl
        
        // Request the data - use UIRefreshController to indicate asynchronous request in progress
        self.refreshControl?.beginRefreshing()
        self.fetchLeagueList()
    }

    // MARK: Private
    
    private func fetchLeagueList() {
        // Asynchronously fetch the league list, on completion update table view or display error message
        self.leagueListViewModel.getLeagueList(completion: { (error) in
            // Stop the refresh control (if it's currently refreshing)
            if (self.refreshControl?.isRefreshing)! {
                self.refreshControl?.endRefreshing()
            }
            // Check the result of the league list fetch
            if error != nil {
                // Error occured, so display an error alert
                let alertTitle = NSLocalizedString("ALERT_CONTROLLER_ERROR_TITLE", comment:"Error")
                let alertMessage = error?.localizedDescription
                self.displayAlertWithTitle(title: alertTitle, message: alertMessage!)
                
            } else {
                // No error, so refresh table view
                self.tableView.reloadData()
            }
        })
    }
    
    // Helper method to display an UIAlertController with the passed in title/message
    private func displayAlertWithTitle(title: String, message: String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("ALERT_CONTROLLER_CANCEL_ACTION_TITLE", comment:"OK"),
                                         style: .cancel, handler: nil)
        
        alert.addAction(cancelAction)
        self.present(alert, animated: true)
    }
    
    // MARK: UIRefreshControl handler
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        // Update the league list
        self.fetchLeagueList()
        // End the refresh control
        refreshControl.endRefreshing()
    }
    
    // MARK: UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.leagueListViewModel.numberOfItemsToDisplay(in: section))
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) ?? UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier: cellReuseIdentifier)
        cell.textLabel!.text = self.leagueListViewModel.titleToDisplay(for: indexPath)
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // On cell selection, check if we have a valid league identifer before transitioning to the
        // Teams List View Controller for the selected league cell
        if let leagueId = self.leagueListViewModel.leagueId(for: indexPath) {
            // Get the league name
            let leagueName = self.leagueListViewModel.leagueName(for: indexPath) ?? ""
            // Initialise the TeamListViewModel (to be injected into Team List View Controller)
            let teamListViewModel = TeamListViewModel(withNetworkService: self.leagueListViewModel.networkService, leagueId: leagueId, leagueName: leagueName)
            // Instantiate the Team List View Controller and inject team list view model
            let teamListViewController = TeamListViewController(withTeamListViewModel: teamListViewModel)
            // Push onto Navigation Controller stack
            self.navigationController?.pushViewController(teamListViewController, animated: true)
            // Clear the UINavigationItem 'back' button text - so you don't see "< Leagues" on the team list view controller
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            // De-select row from table view
            self.tableView.deselectRow(at: indexPath, animated: false)
        }
    }
    
    deinit {
        print("LeagueyListViewController deinit")
    }
}

