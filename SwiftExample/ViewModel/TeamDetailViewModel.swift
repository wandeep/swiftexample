//
//  TeamDetailViewModel.swift
//  SwiftExample
//
//  Created by Wandeep Basra on 10/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import Foundation

/**
 TeamDetailViewModel - supplies data in the required format for the TeamDetailViewController
 */

class TeamDetailViewModel : NSObject {
    
    var name : String
    var webpageURLString : String
    
    // Initializer - inject any dependencies
    init(withName name: String, webpageURLString: String) {
        self.name = name
        self.webpageURLString = webpageURLString
    }
    
    deinit {
        print("TeamDetailViewModel deinit")
    }
}
