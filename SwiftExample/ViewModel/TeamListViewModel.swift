//
//  TeamListViewModel.swift
//  SwiftExample
//
//  Created by Wandeep Basra on 10/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import Foundation

/**
 TeamListViewModel - supplies data in the required format for the TeamListViewController and abstracts any network requests away from the view controller
 */

class TeamListViewModel : NSObject {
    
    // Team list Base URL string
    let teamListBaseURLString = "https://wandeep.vapor.cloud/teams/league/"
    
    // League Id - id of the league to request the teams for - injected
    private var leagueId: Int?

    // League Name - name of the league to request the teams for - injected
    private var leagueName: String?

    // Network service abstracts any netowrking code - injected
    private var networkService: NetworkServiceProtocol
    
    // Array of URLSessionDataTask objects returned from NetworkService asynchronous api request - used for cancellation
    private var dataTasks =  [URLSessionDataTask?]()
    
    // Data for team list view - optional array of Team model objects
    var teams: [Team]?
    
    // Error codes
    enum TeamListViewModelError: Error {
        case invalidURLString
    }
    
    // Initializer - inject any dependencies
    init(withNetworkService networkService: NetworkServiceProtocol, leagueId id: Int, leagueName name: String) {
        self.networkService = networkService
        self.leagueId = id
        self.leagueName = name
    }
    
    // Asynchronous method to request the team list via NetworkService, completion has an Error paramater to indicate success (error is nil) or failure (error is non-nil)
    func getTeamList(completion: @escaping (Error?) -> Void) {
        
        guard let requestURLString = self.requestURLString() else {
            print("Error with requestURLString")
            completion(TeamListViewModelError.invalidURLString)
            return
        }
        
        // Call the NetworkService asynchronous request method to request data - handle appropriately in completion handler
        let dataTask = self.networkService.requestDataFromURLString(urlString: requestURLString) { (optionalTeamDictionary, error) in
            // Create an Error variable to send back in the completin block for this method (getTeamList)
            var errorToReturnInCompletion : Error?
            // Check if there was an error returned from NetworkService
            if (error != nil) {
                // Set error received from NetworkService to local Error object which will returned in completion
                errorToReturnInCompletion = error
            } else {
                // There was no error, so check if we have our team dictionary object
                if let teamDictionary = optionalTeamDictionary {
                    // Initilaise the teams array
                    self.teams = []
                    // Attempt to retrieve the array of Team dictionary items using the "teams" dictionary key
                    if let arrayOfTeams = teamDictionary["teams"] as? [[String : Any]] {
                        // Loop through array and parse dictionary into Team model objects and store in teams array
                        for teamDictionaryItem in arrayOfTeams {
                            let team = Team(withJSONDictionary: teamDictionaryItem)
                            self.teams?.append(team)
                        }
                    }
                }
            }
            // Call the completion block on the main queue to allow the caller (TeamListViewController) to update the UI
            DispatchQueue.main.async {
                completion(errorToReturnInCompletion)
            }
        }
        // Store the data task (for cancellation purposes)
        if let dataTask = dataTask {
            self.dataTasks.append(dataTask)
        }
    }
    
    // Helper method to get the URLString for the request, return nil if error
    private func requestURLString() -> String? {
        if let leagueId = self.leagueId {
            return teamListBaseURLString + "\(leagueId)"
        }
        return nil
    }
    
    // Cancel any outstanding data tasks
    func cancelAllDataTasks() {
        for dataTask in self.dataTasks {
            dataTask?.cancel()
        }
        self.dataTasks.removeAll()
    }
    
    // Return league name for the team
    func leagueNameTitle() -> String {
        return self.leagueName ?? ""
    }
    
    // Return name for team at index path
    func teamName(for indexPath: IndexPath) -> String? {
        return self.teams?[indexPath.row].name
    }
    
    // Return website for team at index path
    func teamWebsite(for indexPath: IndexPath) -> String? {
        return self.teams?[indexPath.row].website
    }
    
    // MARK: - values to display in our table view controller
    
    // Number of rows to be displayed in table view
    func numberOfItemsToDisplay(in section: Int) -> Int {
        return self.teams?.count ?? 0
    }
    
    // Title to to be displayed in table view cell - title label
    func titleToDisplay(for indexPath: IndexPath) -> String {
        return self.teamName(for: indexPath) ?? ""
    }
    
    // Sub-title to to be displayed in table view cell - subtitle label
    func subtitleToDisplay(for indexPath: IndexPath) -> String {
        return self.teamWebsite(for: indexPath) ?? ""
    }
    
    deinit {
        print("TeamListViewModel deinit")
        self.cancelAllDataTasks()
    }
}
