//
//  LeagueListViewModel.swift
//  SwiftExample
//
//  Created by Wandeep Basra on 09/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import Foundation

/**
 LeagueListViewModel - supplies data in the required format for the LeagueListViewController and abstracts any network requests away from the view controller
 */

class LeagueListViewModel : NSObject {
    
    // League list URL string
    let leagueListURLString = "https://wandeep.vapor.cloud/leagues"
    
    // Network service abstracts any netowrking code - injected
    var networkService: NetworkServiceProtocol
    
    // Array of URLSessionDataTask objects returned from NetworkService asynchronous api request - used for cancellation
    var dataTasks =  [URLSessionDataTask?]()
    
    // Data for league list view - optional array of League model objects
    var leagues: [League]?
    
    // Initializer - inject any dependencies
    init(withNetworkService networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    // Asynchronous method to request the league list via NetworkService, completion has an Error paramater to indicate success (error is nil) or failure (error is non-nil)
    func getLeagueList(completion: @escaping (Error?) -> Void) {
        
        // Call the NetworkService asynchronous request method to request data - handle appropriately in completion handler
        let dataTask = self.networkService.requestDataFromURLString(urlString: leagueListURLString) { (optionalLeagueDictionary, error) in
            // Create an Error variable to send back in the completin block for this method (getLeagueList)
            var errorToReturnInCompletion : Error?
            // Check if there was an error
            if (error != nil) {
                // Set error received from NetworkService to local Error object which will returned in completion
                errorToReturnInCompletion = error
            } else {
                // There was no error, so check if we have our league dictionary object
                if let leagueDictionary = optionalLeagueDictionary {
                    // Initilaise the leagues array
                    self.leagues = []
                    // Attempt to retrieve the array of League dictionary items using the "leagues" dictionary key
                    if let arrayOfLeagues = leagueDictionary["leagues"] as? [[String : Any]] {
                        // Loop through array and parse dictionary into League model objects and store in leagues array
                        for leagueDictionaryItem in arrayOfLeagues {
                            let league = League(withJSONDictionary: leagueDictionaryItem)
                            self.leagues?.append(league)
                        }
                    }
                }
            }
            // Call the completion block on the main queue to allow the caller (LeagueListViewController) to update the UI
            DispatchQueue.main.async {
                completion(errorToReturnInCompletion)
            }
        }
        // Store the data task (for cancellation purposes)
        if let dataTask = dataTask {
            self.dataTasks.append(dataTask)
        }
    }
    
    
    // Return name for league at index path
    func leagueName(for indexPath: IndexPath) -> String? {
        return self.leagues?[indexPath.row].name
    }
    
    // Return id for league at index path
    func leagueId(for indexPath: IndexPath) -> Int? {
        return self.leagues?[indexPath.row].id
    }
    
    // MARK: - values to display in our table view controller
    
    // Number of rows to be displayed in table view
    func numberOfItemsToDisplay(in section: Int) -> Int {
        return self.leagues?.count ?? 0
    }
    
    // Title to to be displayed in table view cell - title label
    func titleToDisplay(for indexPath: IndexPath) -> String {
        return self.leagueName(for: indexPath) ?? ""
    }
    
    // Cancel any outstanding data tasks
    func cancelAllDataTasks() {
        for dataTask in self.dataTasks {
            dataTask?.cancel()
        }
        self.dataTasks.removeAll()
    }
    
    deinit {
        print("LeagueListViewModel deinit")
        self.cancelAllDataTasks()
    }
}
