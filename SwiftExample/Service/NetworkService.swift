//
//  NetworkService.swift
//  SwiftExample
//
//  Created by Wandeep Basra on 09/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import Foundation

protocol NetworkServiceProtocol {
    /**
     Asynchronous method to request data from the URL string paramater
     On completion, pass the appropriate data back to caller
     @param urlString The url string to request
     @param completion The closure to be called with result of the request:
     - On success: NSDictionary representation of data, nil error
     - On failure: nil data, Error detailing the reason for failure
     @return An optional URLSessionDataTask; nil on error; non-nil on success (can be used for cancellation)
     */
    func requestDataFromURLString(urlString:String, completion: @escaping (NSDictionary?, Error?) -> Void) -> URLSessionDataTask?
}

// NetworkServiceError codes
public enum NetworkServiceError: Error {
    case invalidURL
    case unableToProcessResponseData
}

// NetworkServiceError localized error description string for appropriate error code
extension NetworkServiceError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .invalidURL:
            return NSLocalizedString("NETWORK_SERVICE_ERROR_INVALID_URL", comment: "Invalid URL")
        case .unableToProcessResponseData:
            return NSLocalizedString("NETWORK_SERVICE_ERROR_UNABLE_TO_PROCESS_RESPONSE_DATA", comment: "Processing Response Data Error")
        }
    }
}

class NetworkService: NSObject, NetworkServiceProtocol {

    func requestDataFromURLString(urlString:String, completion: @escaping (NSDictionary?, Error?) -> Void) -> URLSessionDataTask? {
        
        // Attempt to create URL, if unsuccessful call the completion with appropriate error
        guard let url = URL(string: urlString) else {
            print("Error unwrapping URL")
            completion(nil, NetworkServiceError.invalidURL)
            return nil
        }
        
        // Create a default URLSession and dataTask to get the data
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            
            // Attempt to unwrap the returned response data, if unsuccessful call completion with appropriate error
            guard let unwrappedData = data else {
                print("NetworkService - Error unwrapping data in response")
                completion(nil, NetworkServiceError.unableToProcessResponseData)
                return
            }
            
            // Attempt to serialise the JSON response into our required data format (NSDictionary)
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: unwrappedData, options: .allowFragments) as? NSDictionary {
                    // Successfully serialised JSON response, call completion with our data and nil for the error param
                    completion(responseJSON, nil)
                }
            } catch {
                // Error serilaising JSON, call completion with error
                print("NetworkService - Error serialising JSON rsponse data: \(error.localizedDescription)")
                completion(nil, error)
            }
        }
        // Start the task, by default tasks are created in the suspended state and need to be resumed
        dataTask.resume()
        // Return the task, allows caller to cancel request
        return dataTask
    }
    
    deinit {
        print("NetworkService deinit")
    }
}
