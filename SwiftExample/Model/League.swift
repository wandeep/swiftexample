//
//  League.swift
//  SwiftExample
//
//  Created by Wandeep Basra on 09/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import Foundation

/**
 League model object - contains the name and identifier of the league
 */

struct League {
    let id: Int?
    let name: String?
    
    init(withJSONDictionary jsonDictionary: [String: Any]) {
        id = jsonDictionary["id"] as? Int
        name = jsonDictionary["name"] as? String
    }
}
