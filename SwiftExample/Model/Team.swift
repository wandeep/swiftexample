//
//  Team.swift
//  SwiftExample
//
//  Created by Wandeep Basra on 10/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import Foundation

/**
 Team model object - contains the name, identifier, website and league identifier of the team
 */

struct Team {
    let id: Int?
    let name: String?
    let website: String?
    let league_id: Int?
    
    init(withJSONDictionary jsonDictionary: [String: Any]) {
        id = jsonDictionary["id"] as? Int
        name = jsonDictionary["name"] as? String
        website = jsonDictionary["website"] as? String
        league_id = jsonDictionary["league_id"] as? Int
    }
}
