//
//  NetworkServiceTest.swift
//  SwiftExampleTests
//
//  Created by Wandeep Basra on 09/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import XCTest

@testable import SwiftExample

import OHHTTPStubs

// Create a Mock NetworkService - will be used by other unit tests that have a dependency on the NetworkServiceProtocol
class MockNetworkService : NetworkServiceProtocol {
    
    func requestDataFromURLString(urlString: String, completion: @escaping (NSDictionary?, Error?) -> Void) -> URLSessionDataTask? {
        // Call the completion immediately
        let dictionary : NSDictionary = [:]
        let error : Error? = nil
        completion(dictionary, error)
        return nil
    }
}

class NetworkServiceTest: XCTestCase {
    
    var SUT : NetworkService?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.SUT = NetworkService()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNetworkServiceRequestingLeagueDataSuccess() {
        
        // Define an expectation
        let urlExpect = expectation(description: "NetworkService does request and runs the callback closure")
        
        let testURL = "https://wandeep.vapor.cloud/leagues"
        
        // Stub the network response - use the stubbed response JSON file in the bundle
        stub(condition: isHost("testURL") ) { _ in
            return OHHTTPStubsResponse(
                fileAtPath: OHPathForFile("StubbedLeagueList.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type":"application/json"]
            )
        }
        
        // Exercise the asynchronous code
        let dataTask: URLSessionDataTask? = self.SUT?.requestDataFromURLString(urlString: testURL, completion: { (dictionaryObject, error) in
            // Assert the params returned in completion handler
            XCTAssertNotNil(dictionaryObject)
            XCTAssertNil(error)
            
            // Don't forget to fulfill the expectation in the async callback
            urlExpect.fulfill()
        })
        // Assert the returned URLSessionDataTask is not nil
        XCTAssertNotNil(dataTask)
        
        // Wait for the expectation to be fulfilled
        waitForExpectations(timeout: 0.5, handler: { (error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        })
        
    }
    
    func testNetworkServiceRequestingTeamDataSuccess() {
        
        // Define an expectation
        let urlExpect = expectation(description: "NetworkService does request and runs the callback closure")
        
        let testURL = "https://wandeep.vapor.cloud/teams/league/1"
        
        // Stub the network response - use the stubbed response JSON file in the bundle
        stub(condition: isHost("testURL") ) { _ in
            return OHHTTPStubsResponse(
                fileAtPath: OHPathForFile("StubbedTeamList.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type":"application/json"]
            )
        }
        
        // Exercise the asynchronous code
        let dataTask: URLSessionDataTask? = self.SUT?.requestDataFromURLString(urlString: testURL, completion: { (dictionaryObject, error) in
            // Assert the params returned in completion handler
            XCTAssertNotNil(dictionaryObject)
            XCTAssertNil(error)
            
            // Don't forget to fulfill the expectation in the async callback
            urlExpect.fulfill()
        })
        // Assert the returned URLSessionDataTask is not nil
        XCTAssertNotNil(dataTask)
        
        // Wait for the expectation to be fulfilled
        waitForExpectations(timeout: 0.5, handler: { (error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        })
        
    }
    
    func testNetworkServiceHandlesAnUnderlyingError() {
        
        // Define an expectation
        let urlExpect = expectation(description: "NetworkService does request and runs the callback closure")
        
        let testURL = "https://testURL"
        
        // Stub the network response - simulate an error
        stub(condition: isHost("testURL") ) { _ in
            return OHHTTPStubsResponse(
                error: NSError(domain:"Error Domain", code:-1))
        }
        
        // Exercise the asynchronous code
        let dataTask: URLSessionDataTask? = self.SUT?.requestDataFromURLString(urlString: testURL, completion: { (dictionaryObject, error) in
            // Assert the params returned in completion handler
            XCTAssertNil(dictionaryObject)
            XCTAssertNotNil(error)
            
            // Don't forget to fulfill the expectation in the async callback
            urlExpect.fulfill()
        })
        // Assert the returned URLSessionDataTask is not nil
        XCTAssertNotNil(dataTask)
        
        // Wait for the expectation to be fulfilled
        waitForExpectations(timeout: 0.5, handler: { (error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        })
        
    }
}

