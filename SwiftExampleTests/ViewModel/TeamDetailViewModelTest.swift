//
//  TeamDetailViewModelTest.swift
//  SwiftExampleTests
//
//  Created by Wandeep Basra on 10/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import XCTest

@testable import SwiftExample

class TeamDetailViewModelTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitialization() {
        // Create name and webpage URL string properties to be injected into Team Detail View Model
        let name = "name"
        let webpageURLString = "webpageURLString"
        
        // Initialize Team Detail View Model with name and webpageURLString
        let teamDetailViewModel = TeamDetailViewModel(withName: name, webpageURLString: webpageURLString)
        
        // Assert the initialization and name/webpage attributes have been injected
        XCTAssertNotNil(teamDetailViewModel, "The team detail view model should not be nil.")
        XCTAssertTrue(teamDetailViewModel.name == name, "The name should be equal to the name that was passed in.")
        XCTAssertTrue(teamDetailViewModel.webpageURLString == webpageURLString, "The webpageURLString should be equal to the webpageURLString that was passed in.")
        
    }
    
}

