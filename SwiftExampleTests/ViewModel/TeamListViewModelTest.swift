//
//  TeamListViewModelTest.swift
//  SwiftExampleTests
//
//  Created by Wandeep Basra on 10/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import XCTest

@testable import SwiftExample


class TeamListViewModelTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitialization() {
        // Create network service to be injected into Team List View Model
        let mockNetworkService : NetworkServiceProtocol = MockNetworkService()
        
        // Initialize Team List View Model with network service and League id
        let teamListViewModel = TeamListViewModel(withNetworkService: mockNetworkService, leagueId: 1, leagueName: "" )
        
        // Assert the initialization
        XCTAssertNotNil(teamListViewModel, "The team list view model should not be nil.")
    }
    
    func testGetTeamListAsynchronousMethod() {
        // Create network service to be injected into League List View Model
        let mockNetworkService = MockNetworkService()
        
        // Initialize Team List View Model with network service
        let teamListViewModel = TeamListViewModel(withNetworkService: mockNetworkService, leagueId: 1, leagueName: "")
        
        // Define an expectation
        let urlExpect = expectation(description: "SomeService does stuff and runs the callback closure")
        
        // Exercise the asynchronous code
        teamListViewModel.getTeamList { (error) in
            print("getTeamList completion called")
            XCTAssertNil(error)
            
            // Don't forget to fulfill the expectation in the async callback
            urlExpect.fulfill()
        }
        
        // Wait for the expectation to be fulfilled
        waitForExpectations(timeout: 0.5, handler: { (error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        })
    }
    
    func testHelperMethodsReturnCorrectAttributesFromTeamList() {
        
        // Create a sample list of Team model objects
        let teamCount = 10
        var teams = [Team]()
        for i in 0..<teamCount {
            let team = self.team(withName: "Name:\(i)", id: i, website: "Website:\(i)", leagueId: i)
            // Append the team to the array
            teams.append(team)
        }
        
        // Initialize SUT (System Under Test) i.e. Team List View Model
        let leagueId = 1
        let leagueName = "test league"
        
        let SUT = TeamListViewModel(withNetworkService: NetworkService(), leagueId: leagueId, leagueName: leagueName)
        
        // Initilaise the teams array with our sample list of known Team model objects
        SUT.teams = teams
        
        // Assert the helper methods for a given NSIndexPath
        let rowIndex = 1
        let sectionIndex = 0
        let indexPath = NSIndexPath(row: rowIndex, section: sectionIndex)
        
        XCTAssertTrue(SUT.teamName(for: indexPath as IndexPath) == "Name:\(rowIndex)" , "The teamName should be equal to the name of the team at the specified index path.")
        
        XCTAssertTrue(SUT.teamWebsite(for: indexPath as IndexPath) == "Website:\(rowIndex)" , "The teamWebsite should be equal to the website of the team at the specified index path.")
        
        XCTAssertTrue(SUT.leagueNameTitle() == leagueName, "The leagueNameTitle() should return \(leagueName).")
        
        // Table View Helper methods
        
        XCTAssertTrue(SUT.numberOfItemsToDisplay(in: sectionIndex) == teamCount , "The numberOfItemsToDisplay should be equal to \(teamCount).")
        
        XCTAssertTrue(SUT.titleToDisplay(for: indexPath as IndexPath) == "Name:\(rowIndex)" , "The titleToDisplay should be equal to the name of the team at the specified index path.")
        
        XCTAssertTrue(SUT.subtitleToDisplay(for: indexPath as IndexPath) == "Website:\(rowIndex)" , "The subtitleToDisplay should be equal to the website of the team at the specified index path.")
    }
    
    // Helper method to return a Team model object populated with parameters passed in
    private func team(withName name: String, id: Int, website: String, leagueId: Int) -> Team {
        
        // Valid JSON Dictionary keys
        let jsonDictKeyId = "id"
        let jsonDictKeyName = "name"
        let jsonDictKeyWebsite = "website"
        let jsonDictKeyLeagueId = "league_id"
        
        // Create a sample JSON dictionary to be injected into the Team model object initializer
        let leagueJSONDictionary : [String: Any] = [
            jsonDictKeyId : id,
            jsonDictKeyName : name,
            jsonDictKeyWebsite : website,
            jsonDictKeyLeagueId : leagueId
        ]
        
        // Create the team model object
        let team = Team(withJSONDictionary: leagueJSONDictionary)
        
        return team
    }
}
