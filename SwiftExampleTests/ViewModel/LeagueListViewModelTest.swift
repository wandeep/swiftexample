//
//  LeagueListViewModelTest.swift
//  SwiftExampleTests
//
//  Created by Wandeep Basra on 09/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import XCTest

@testable import SwiftExample

class LeagueListViewModelTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitialization() {
        // Create network service to be injected into League List View Model
        let mockNetworkService : NetworkServiceProtocol = MockNetworkService()
        
        // Initialize League List View Model with network service
        let leagueListViewModel = LeagueListViewModel(withNetworkService: mockNetworkService)
        
        // Assert the initialization
        XCTAssertNotNil(leagueListViewModel, "The league list view model should not be nil.")
    }
    
    func testGetLeagueListAsynchronousMethod() {
        // Create network service to be injected into League List View Model
        let mockNetworkService = MockNetworkService()
        
        // Initialize League List View Model with network service
        let leagueListViewModel = LeagueListViewModel(withNetworkService: mockNetworkService)
        
        // Define an expectation
        let urlExpect = expectation(description: "SomeService does stuff and runs the callback closure")
        
        // Exercise the asynchronous code
        leagueListViewModel.getLeagueList { (error) in
            print("completion called")
            XCTAssertNil(error)
            
            // Don't forget to fulfill the expectation in the async callback
            urlExpect.fulfill()
        }
        
        // Wait for the expectation to be fulfilled
        waitForExpectations(timeout: 0.5, handler: { (error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        })
    }
    
    func testHelperMethodsReturnCorrectAttributesFromLeagueList() {
        
        // Create a sample list of League model objects
        let leagueCount = 3
        var leagues = [League]()
        for i in 0..<leagueCount {
            let league = self.league(withName: "Name:\(i)", id: i)
            // Append the league to the array
            leagues.append(league)
        }
        
        // Initialize SUT (System Under Test) i.e. League List View Model
        let SUT = LeagueListViewModel(withNetworkService: NetworkService())
        
        // Initilaise the leagues array with our sample list of known League model objects
        SUT.leagues = leagues
        
        // Assert the helper methods for a given NSIndexPath
        let rowIndex = 1
        let sectionIndex = 0
        let indexPath = NSIndexPath(row: rowIndex, section: sectionIndex)
        
        XCTAssertTrue(SUT.leagueName(for: indexPath as IndexPath) == "Name:\(rowIndex)" , "The leagueName should be equal to the name of the league at the specified index path.")
        
        XCTAssertEqual(SUT.leagueId(for: indexPath as IndexPath), rowIndex, "The leagueId should be equal to the id of the league at the specified index path." )
        
        // Table View Helper methods
        
        XCTAssertTrue(SUT.numberOfItemsToDisplay(in: sectionIndex) == leagueCount , "The numberOfItemsToDisplay should be equal to \(leagueCount).")
        
        XCTAssertTrue(SUT.titleToDisplay(for: indexPath as IndexPath) == "Name:\(rowIndex)" , "The titleToDisplay should be equal to the name of the league at the specified index path.")
    }
    
    // Helper method to return a League model object populated with parameters passed in
    private func league(withName name: String, id: Int) -> League {
        
        // Valid JSON Dictionary keys
        let jsonDictKeyId = "id"
        let jsonDictKeyName = "name"
        
        // Create a sample JSON dictionary to be injected into the League model object initializer
        let leagueJSONDictionary : [String: Any] = [
            jsonDictKeyId : id,
            jsonDictKeyName : name
        ]
        
        // Create the league model object
        let league = League(withJSONDictionary: leagueJSONDictionary)
        
        return league
    }
}

