//
//  TeamTest.swift
//  SwiftExampleTests
//
//  Created by Wandeep Basra on 10/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import XCTest

@testable import SwiftExample

class TeamTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitialization() {
        // Valid JSON Dictionary keys
        let jsonDictKeyId = "id"
        let jsonDictKeyName = "name"
        let jsonDictKeyWebsite = "website"
        let jsonDictKeyLeagueId = "league_id"
        
        // Create a sample JSON dictionary to be injected into the Team model object
        let teamJSONDictionary : [String: Any] = [
            jsonDictKeyId : 1,
            jsonDictKeyName : "Test Team",
            jsonDictKeyWebsite: "Test Website",
            jsonDictKeyLeagueId : 1
        ]
        
        // Initialize Team model with JSON dictionary
        let team = Team(withJSONDictionary: teamJSONDictionary)
        
        // Assert the initialization
        XCTAssertNotNil(team, "The team model object should not be nil.")
        
        // Assert the attributes specified in the JSON dictionary are the same in the Team model object
        XCTAssertTrue(team.id == teamJSONDictionary[jsonDictKeyId] as? Int, "The team id should be equal to the id that was passed in via JSON dictionary.")
        
        XCTAssertTrue(team.name == teamJSONDictionary[jsonDictKeyName] as? String, "The team name should be equal to the name that was passed in via JSON dictionary.")
        
        XCTAssertTrue(team.website == teamJSONDictionary[jsonDictKeyWebsite] as? String, "The team website should be equal to the website that was passed in via JSON dictionary.")

        XCTAssertTrue(team.league_id == teamJSONDictionary[jsonDictKeyLeagueId] as? Int, "The league id should be equal to the league id that was passed in via JSON dictionary.")
    }
    
}
