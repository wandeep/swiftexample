//
//  LeagueTest.swift
//  SwiftExampleTests
//
//  Created by Wandeep Basra on 09/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//
import XCTest

@testable import SwiftExample

class LeagueTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitialization() {
        // Valid JSON Dictionary keys
        let jsonDictKeyId = "id"
        let jsonDictKeyName = "name"
        
        // Create a sample JSON dictionary to be injected into the League model object
        let leagueJSONDictionary : [String: Any] = [
            jsonDictKeyId : 1,
            jsonDictKeyName : "Test Team"
        ]
        
        // Initialize League model with JSON dictionary
        let league = League(withJSONDictionary: leagueJSONDictionary)
        
        // Assert the initialization
        XCTAssertNotNil(league, "The league model object should not be nil.")
        
        // Assert the attributes specified in the JSON dictionary are the same in the League model object
        XCTAssertTrue(league.id == leagueJSONDictionary[jsonDictKeyId] as? Int, "The league id should be equal to the id that was passed in via JSON dictionary.")
        
        XCTAssertTrue(league.name == leagueJSONDictionary[jsonDictKeyName] as? String, "The league name should be equal to the name that was passed in via JSON dictionary.")
        
    }
    
}
