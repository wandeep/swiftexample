**SwiftExample**

A sample app written in Swift utilising the MVVM (Model-View-ViewModel) architecture which requests and displays data from the web backend provided by [VaporExample](https://bitbucket.org/wandeep/vaporexample) (also written in Swift using Vapor web framework).

The sample app includes unit tests and UI tests (XCTest).