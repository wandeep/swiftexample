//
//  SwiftExampleUITests.swift
//  SwiftExampleUITests
//
//  Created by Wandeep Basra on 07/03/2018.
//  Copyright © 2018 Wandeep Basra. All rights reserved.
//

import XCTest

class SwiftExampleUITests: XCTestCase {
    
    let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLeaguesViewIsLoaded() {
        // Since the league list view is loaded asynchronously, wait for an expectation prior to asserting
        // Wait for the League List View Controller's table view to display a cell containing one of the leagues
        // e.g. "Premier League"
        let label = app.tables.staticTexts["Premier League"]
        let exists = NSPredicate(format: "exists == 1")
        
        expectation(for: exists, evaluatedWith: label, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)

        // Assert the navigation controller is dosplaying the League List View Controller (i.e. shows "Leagues")
        XCTAssertEqual(app.navigationBars.element.identifier, "Leagues")
    }
}
